﻿const express = require("express");
const app = express();
const bodyParser = require("body-parser")

const userRouter = require("./routes/userRouter.js")
const homeRouter = require("./routes/homeRouter.js")

app.set("view engine", "hbs")
app.use(bodyParser.urlencoded({extended: false}))

// сопоcтавляем роутер с конечной точкой "/users"
app.use("/users", userRouter);
app.use("/", homeRouter)
 
// обработка ошибки 404
app.use(function (req, res, next) {
    res.status(404).send("Not Found")
});
 
app.listen(3000, ()=>{console.log("Server has been running...")});

