﻿const db = require('../db/db.js')
//const users = [];
 
module.exports= class User{
 
    constructor(name, age){
        this.name = name;
        this.age = age;
    }
    async save(){
        //users.push(this);
        const newUser = await db.query('INSERT INTO users (name, age) values ($1, $2) RETURNING *', [this.name, this.age]);
        return newUser;
    }
    static async getAll(){
        //return users;
        const allUsers = await db.query('SELECT * FROM users;')
        return allUsers.rows;
    }
}